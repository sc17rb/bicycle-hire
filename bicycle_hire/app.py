from flask import Flask
from bicycle_hire.blueprints.bicycle.views import bicycles
from bicycle_hire.blueprints.user.views import users
from bicycle_hire.blueprints.user.models import User
from bicycle_hire.blueprints.payment.views import payments
from bicycle_hire.blueprints.statistics.views import stats
from bicycle_hire.extensions import ( debug_toolbar, db, login_manager, google_maps, mail )


def create_app(settings_override=None):

    app = Flask(__name__, instance_relative_config=True)

    app.config.from_object('config.settings')
    app.config.from_pyfile('settings.py', silent=True)

    if settings_override:
        app.config.update(settings_override)

    app.register_blueprint(bicycles)
    app.register_blueprint(users)
    app.register_blueprint(payments)
    app.register_blueprint(stats)
    extensions(app)
    auth()

    return app


# Register the extensions
def extensions(app):

    debug_toolbar.init_app(app)
    db.init_app(app)
    google_maps.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    return None


# Required by Flask-Login
def auth():
    login_manager.login_view = "user.signin"

    # loads (from the session) and returns a user object
    @login_manager.user_loader
    def load_user(user_id):
        return User.query.get(user_id)
