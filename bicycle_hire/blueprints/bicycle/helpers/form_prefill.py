def prefill_booking_form(form, form_model, index, no_bikes=False, email=False):
    # Prefill index/filter form
    if index:
        form.location.data = form_model.location_name
        form.start_date.data = form_model.start_date_time.date()
        form.start_time.data = str(int(form_model.start_date_time.time().strftime('%H')))
        form.end_date.data = form_model.end_date_time.date()
        form.end_time.data = str(int(form_model.end_date_time.time().strftime('%H')))
    
    # Prefill filter form
    if form_model.number_of_bikes and no_bikes:
        form.number_of_bikes.data = form_model.number_of_bikes
    
    # Prefill email form
    if form_model.email and email:
        form.email.data = form_model.email