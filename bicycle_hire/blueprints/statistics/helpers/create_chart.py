from bokeh.plotting import figure
from bokeh.embed import components
from bokeh.models.sources import ColumnDataSource
from bokeh.transform import dodge
from bokeh.core.properties import value


# Generates the information needed to create the bicyles per location chart
def create_booking_chart(location, values):
    # Create initial graph
    plot = figure(title = "Bicycle Bookings", plot_height = 500, x_range = location,
                  toolbar_location = None, tools = "")

    # Add values into the graph
    plot.vbar(x=location, top = values, width = 0.9)

    plot.xgrid.grid_line_color = None
    plot.y_range.start = 0

    return components(plot)


# Generates the information needed to create the weekly income chart
def create_income_chart(locations,income):

    total_income = 0.0
    total_list = []

    # Sums the income of all locations
    for key in income:
        total_income += float(key)

    # Appends a total income for each location
    for key in income:
        total_list.append(total_income)

    data = {"Locations":locations, "Income":income, "Total Income":total_list}
    source = ColumnDataSource(data=data)

    p = figure(title = "Weekly Income", plot_height = 500, x_range = locations,
              y_range=(0.0, total_income + 1000.0), toolbar_location = None, tools = "")

    p.vbar(x=dodge('Locations', 0.0, range=p.x_range), top = 'Income',
           color="#718dbf", width = 0.2, source=source, legend=value("Income"))

    p.vbar(x=dodge('Locations', 0.25, range=p.x_range), top = 'Total Income',
           width = 0.2, source=source, legend=value("Total Income"))

    p.x_range.range_padding = 0.1

    p.legend.location = "top_left"
    p.legend.orientation = "horizontal"
    p.xgrid.grid_line_color = None
    p.y_range.start = 0

    return components(p)
