from flask import (Blueprint,
                   render_template,
                   flash,
                   url_for,
                   redirect,
                   request,
                   abort)
from flask_login import (current_user,
                         login_required
                        )
from bicycle_hire.blueprints.user.helpers.decorators import staff_login_required
from bicycle_hire.blueprints.statistics.forms import BookedBikesForm, WeekForm
from bicycle_hire.blueprints.bicycle.models import Booking, Location
from bicycle_hire.blueprints.payment.models import Payment
from bicycle_hire.blueprints.statistics.helpers.create_chart import create_booking_chart
from bicycle_hire.blueprints.statistics.helpers.create_chart import create_income_chart


stats = Blueprint('stats', __name__, template_folder='templates', url_prefix='/statistics')


@stats.route('weekly_income', methods=['GET', 'POST'])
@login_required
@staff_login_required()
def weekly_income():
    form = WeekForm()
    # Set the initial graph to none
    income_per_loc = None
    plot = None
    script = None
    div = None

    if form.validate_on_submit():
        if form.submit.data:

            # Get the initial income dictionary from database
            week = int(form.week.data)
            income_per_loc = Payment.get_weekly_income(week)

            # Convert the dictionary into two lists
            locations = list(income_per_loc.keys())
            income = list(income_per_loc.values())

            # Create the graph
            script, div = create_income_chart(locations, income)

    return render_template('statistics/weekly_income.html',
                            form = form,
                            script = script,
                            div = div
                          )


@stats.route('bicycles_booked', methods=['GET', 'POST'])
@login_required
@staff_login_required()
def bicycles_booked():
    form = BookedBikesForm()
    bicycle_per_loc = None
    plot = None
    script = None
    div = None

    if form.validate_on_submit():
        if form.submit.data:

            start_date = form.start_date.data
            end_date = form.end_date.data

            bicycle_per_loc = Booking.get_bike_stats(start_date, end_date)

            location = list(bicycle_per_loc.keys())
            values = list(bicycle_per_loc.values())

            script, div = create_booking_chart(location, values)

    return render_template('statistics/bicycles_booked.html',
                            form = form,
                            script = script,
                            div = div
                          )
