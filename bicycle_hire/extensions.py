from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_googlemaps import GoogleMaps
from flask_mail import Mail

debug_toolbar = DebugToolbarExtension()
db = SQLAlchemy()
google_maps = GoogleMaps()
login_manager = LoginManager()
mail = Mail()
