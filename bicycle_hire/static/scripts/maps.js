// adds the map onto the website
function addMap() {
    var mapProp = new google.maps.LatLng(53.7965, 1.5438);

    var map = new google.maps.Map(document.getElementById("map_container"),{
        center:mapProp,
        zoom:5
    });

// sets the marker on the map based on the coordinates given
    var MapMarker = new google.maps.Marker({
      position: mapProp,
      title: 'Bike Rental Shops',
      map:map
    });
}

$(document).ready(function(){
    initialize();
});
