from flask import url_for

# Tests the webpages in the user/views.py to see if they respond properly
class TestUserViews(object):
    def test_signup_page(self, client):
        response = client.get(url_for('user.signup'))
        assert response.status_code == 200

    def test_signin_page(self, client):
        response = client.get(url_for('user.signin'))
        assert response.status_code == 200

    def test_signout_page(self, client):
        response = client.get(url_for('user.signout'))
        # Status code for redirecting to another site after signing out
        assert response.status_code == 302

